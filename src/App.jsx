import { Navigate, Route, Routes } from "react-router-dom";
import Authentication from "./features/authentication/pages";
import { useSelector } from "react-redux";
import Home from "./features/Home/pages";

function App() {
  const {
    authentication: { user },
  } = useSelector((state) => state);

  return (
    <>
      <Routes>
        {user ? (
          <Route path="/" element={<Home />} />
        ) : (
          <Route path="/" element={<Navigate to="/authentication" />} />
        )}
        <Route path="/authentication/*" element={<Authentication />} />
      </Routes>
    </>
  );
}

export default App;
