import { LOGIN, LOGOUT } from "../constants";

const initialState = {
  user: null,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      const { email } = action.payload;
      return { ...state, user: { email } };
    case LOGOUT:
      return { ...state, user: null };
    default:
      return state;
  }
};

export default reducer;
