import { combineReducers } from "redux";
import authenticationReducer from "./authenticationReducer";

const reducers = combineReducers({
  authentication: authenticationReducer,
});

export default reducers;
