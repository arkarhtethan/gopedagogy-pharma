import { LOGIN, LOGOUT } from "../constants";

export const loginAction = (loginData) => {
  return (dispatch) => {
    dispatch({
      type: LOGIN,
      payload: loginData,
    });
  };
};

export const logoutAction = () => {
  return (dispatch) => {
    dispatch({
      type: LOGOUT,
    });
  };
};
