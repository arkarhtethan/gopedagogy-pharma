import React from "react";
import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import * as authActionCreators from "../../../store/actions/authActions";
const Home = () => {
  const dispatch = useDispatch();

  const { logoutAction } = bindActionCreators(authActionCreators, dispatch);

  return (
    <div className="App">
      <button onClick={() => logoutAction()}>Logout</button>
    </div>
  );
};

export default Home;
