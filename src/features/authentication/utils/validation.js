export const isErrorObjectEmpty = (errors = {}, ...rest) => {
  return (
    Object.keys(errors).length === 0 &&
    rest.every((item) => item && item !== "")
  );
};
