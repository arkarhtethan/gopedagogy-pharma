import * as yup from "yup";

export const ChangePasswordSchema = yup.object().shape({
  currentPassword: yup
    .string()
    .min(4, "Password must contain at least 4 characters")
    .required("Password is required !"),
  newPassword: yup
    .string()
    .min(4, "Password must contain at least 4 characters")
    .required("Password is required !"),
  confirmNewPassword: yup
    .string()
    .min(4, "Password must contain at least 4 characters")
    .required("Password is required !"),
});
