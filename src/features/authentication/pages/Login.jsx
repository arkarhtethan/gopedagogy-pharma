import { Box, Checkbox, Container, Paper, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import EmailInput from "../components/EmailInput";
import PasswordInput from "../components/PasswordInput";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { LoginSchema } from "../schema/loginSchema";
import { isErrorObjectEmpty } from "../utils/validation";
import SubmitButton from "../components/SubmitButton";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import * as authActionCreator from "../../../store/actions/authActions";

const useStyles = makeStyles((theme) => ({
  container: {
    margin: "auto",
    paddingTop: "9%",
  },
  formContiner: {
    margin: "auto",
    padding: theme.spacing(4),
    width: 500,
    [theme.breakpoints.down("sm")]: {
      backgroundColor: "red",
      margin: theme.spacing(1),
      width: "95%",
      left: 0,
      padding: theme.spacing(2),
    },
  },
  icon: {
    width: 2,
  },
  header: {
    marginBottom: theme.spacing(3),
  },
  bottomText: {
    marginTop: theme.spacing(3),
    textAlign: "center",
    fontSize: "0.2em",
  },
  forgotPasswordContainer: {
    marginBottom: theme.spacing(3),
    display: "flex",
    alignItmes: "center",
    justifyContent: "space-between",
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: "none",
  },
}));

const Login = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(LoginSchema),
  });
  const { user } = useSelector((state) => state.authentication);
  const dispatch = useDispatch();

  const { loginAction } = bindActionCreators(authActionCreator, dispatch);

  const { email, password } = getValues();

  useEffect(() => {
    if (user) {
      navigate("/");
    }
  }, [user, navigate]);

  const submitForm = () => {
    loginAction({ email });
    navigate("/home");
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.formContiner} elevation={3}>
        <Box className={classes.header}>
          <Typography variant="h5" component="h1" gutterBottom>
            Login
          </Typography>
          <Typography variant="body2" component="h1" gutterBottom>
            Login your account!
          </Typography>
        </Box>
        <form onSubmit={handleSubmit(submitForm)}>
          <Box>
            <EmailInput
              register={register}
              name="email"
              error={errors.email?.message}
            />
          </Box>
          <Box sx={{ marginY: 3 }}>
            <PasswordInput
              showPassword={showPassword}
              setShowPassword={setShowPassword}
              register={register}
              name="password"
              error={errors.password?.message}
            />
          </Box>
          <Box className={classes.forgotPasswordContainer}>
            <Box
              sx={{
                display: "flex",
                alignItmes: "center",
                justifyContent: "space-between",
              }}
            >
              <Checkbox defaultChecked sx={{ height: 10 }} />
              <Typography variant="body2">Remember Password</Typography>
            </Box>
            <Typography variant="body2">
              <Link
                className={classes.link}
                to="/authentication/forgot-password"
              >
                Forgot Password
              </Link>
            </Typography>
          </Box>
          <SubmitButton
            disabled={!isErrorObjectEmpty(errors, email, password)}
            text="Login"
          />
        </form>
        <Box className={classes.bottomText}>
          <Typography variant="body2">
            Don't have an account?{" "}
            <Link className={classes.link} to="/authentication/register">
              Register
            </Link>
          </Typography>
        </Box>
      </Paper>
    </Container>
  );
};

export default Login;
