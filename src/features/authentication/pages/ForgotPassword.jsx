import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import { Box, Paper, Typography, Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useForm } from "react-hook-form";
import EmailInput from "../components/EmailInput";
import SubmitButton from "../components/SubmitButton";
import { ResetSchema } from "../schema/resetSchema";
import { isErrorObjectEmpty } from "../utils/validation";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
    height: "100vh",
    margin: "auto",
  },
  formContiner: {
    position: "absolute",
    top: "30%",
    left: "28%",
    padding: theme.spacing(4),
    width: 500,
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(1),
      width: "95%",
      left: 0,
      padding: theme.spacing(2),
    },
  },
  emailContainer: {
    marginBottom: theme.spacing(3),
  },
  header: {
    marginBottom: theme.spacing(3),
  },
}));

const ForgotPassword = () => {
  const navigate = useNavigate();
  const classes = useStyles();
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(ResetSchema),
  });

  const { email } = watch();

  const submitForm = () => {
    navigate("/authentication/change-password");
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.formContiner} elevation={3}>
        <Box className={classes.header}>
          <Typography variant="h5" component="h1" gutterBottom>
            Forgot Password
          </Typography>
        </Box>
        <form onSubmit={handleSubmit(submitForm)}>
          <Box className={classes.emailContainer}>
            <EmailInput
              register={register}
              name="email"
              error={errors.email?.message}
            />
          </Box>
          <SubmitButton
            text="Reset Password"
            disabled={!isErrorObjectEmpty(errors, email)}
          />
        </form>
      </Paper>
    </Container>
  );
};

export default ForgotPassword;
