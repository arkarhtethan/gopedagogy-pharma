import { yupResolver } from "@hookform/resolvers/yup";
import { Box, Container, Paper, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import EmailInput from "../components/EmailInput";
import PasswordInput from "../components/PasswordInput";
import SubmitButton from "../components/SubmitButton";
import { RegisterSchema } from "../schema/registerSchema";
import { isErrorObjectEmpty } from "../utils/validation";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: "5%",
    margin: "auto",
  },
  formContiner: {
    margin: "auto",
    padding: theme.spacing(4),
    width: 500,
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(1),
      width: "95%",
      left: 0,
      padding: theme.spacing(2),
    },
  },
  icon: {
    fontSize: 6,
  },
  header: {
    marginBottom: theme.spacing(3),
  },
  bottomText: {
    marginTop: theme.spacing(3),
    textAlign: "center",
  },
  link: {
    color: theme.palette.secondary.main,
    textDecoration: "none",
  },
}));

const Register = () => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
    watch,
    clearErrors,
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(RegisterSchema),
  });
  const { email, password, confirmPassword } = watch();
  const navigate = useNavigate();

  useEffect(() => {
    if (
      password !== "" &&
      confirmPassword !== "" &&
      password !== confirmPassword
    ) {
      setError("password", {
        type: "manual",
        message: "Password do not match.",
      });
    } else {
      const error = errors["password"];
      if (error && error["type"] && error["type"] === "manual") {
        clearErrors("password");
      }
    }
  }, [password, confirmPassword, clearErrors, errors, setError]);

  const submitForm = () => {
    navigate("/authentication/login");
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.formContiner} elevation={3}>
        <Box className={classes.header}>
          <Typography variant="h5" component="h1" gutterBottom>
            Register
          </Typography>
          <Typography variant="body2" component="h1" gutterBottom>
            Create your account!
          </Typography>
        </Box>
        <form onSubmit={handleSubmit(submitForm)}>
          <Box>
            <EmailInput
              register={register}
              name="email"
              error={errors.email?.message}
            />
          </Box>
          <Box sx={{ marginY: 3 }}>
            <PasswordInput
              showPassword={showPassword}
              setShowPassword={setShowPassword}
              register={register}
              name="password"
              error={errors.password?.message}
            />
          </Box>
          <Box sx={{ marginY: 3 }}>
            <PasswordInput
              label="Confirm Password"
              placeholder="Enter your confirm password"
              showPassword={showConfirmPassword}
              setShowPassword={setShowConfirmPassword}
              register={register}
              name="confirmPassword"
              error={errors.confirmPassword?.message}
            />
          </Box>
          <SubmitButton
            disabled={
              !isErrorObjectEmpty(errors, email, password, confirmPassword)
            }
            text="Register"
          />
        </form>
        <Box className={classes.bottomText}>
          <Typography variant="body2">
            Already have an account?{" "}
            <Link className={classes.link} to="/authentication/login">
              Login
            </Link>
          </Typography>
        </Box>
      </Paper>
    </Container>
  );
};

export default Register;
