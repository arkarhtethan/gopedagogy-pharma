import { yupResolver } from "@hookform/resolvers/yup";
import { Box, Container, Paper, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import PasswordInput from "../components/PasswordInput";
import SubmitButton from "../components/SubmitButton";
import { ChangePasswordSchema } from "../schema/changePasswordSchema";
import { isErrorObjectEmpty } from "../utils/validation";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: "5%",
    margin: "auto",
  },
  formContiner: {
    margin: "auto",
    padding: theme.spacing(4),
    width: 500,
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(1),
      width: "95%",
      left: 0,
      padding: theme.spacing(2),
    },
  },
  icon: {
    fontSize: 6,
  },
  header: {
    marginBottom: theme.spacing(3),
  },
  bottomText: {
    marginTop: theme.spacing(3),
    textAlign: "center",
  },
  link: {
    color: theme.palette.secondary.main,
    textDecoration: "none",
  },
}));

const ChangePassword = () => {
  const classes = useStyles();
  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmNewPassword, setShowConfirmNewPassword] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
    watch,
    clearErrors,
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(ChangePasswordSchema),
  });
  const { currentPassword, newPassword, confirmNewPassword } = watch();

  const navigate = useNavigate();

  useEffect(() => {
    if (
      newPassword !== "" &&
      confirmNewPassword !== "" &&
      newPassword !== confirmNewPassword
    ) {
      setError("newPassword", {
        type: "manual",
        message: "Password do not match.",
      });
    } else {
      const error = errors["newPassword"];
      if (error && error["type"] && error["type"] === "manual") {
        clearErrors("newPassword");
      }
    }
  }, [newPassword, confirmNewPassword, clearErrors, errors, setError]);

  const submitForm = () => {
    navigate("/authentication/login");
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.formContiner} elevation={3}>
        <Box className={classes.header}>
          <Typography variant="h5" component="h1" gutterBottom>
            Change Password
          </Typography>
        </Box>
        <form onSubmit={handleSubmit(submitForm)}>
          <Box>
            <PasswordInput
              showPassword={showCurrentPassword}
              setShowPassword={setShowCurrentPassword}
              register={register}
              name="currentPassword"
              label="Current Password"
              error={errors.currentPassword?.message}
            />
          </Box>
          <Box sx={{ marginY: 3 }}>
            <PasswordInput
              label="New Password"
              placeholder="Enter your new password"
              showPassword={showNewPassword}
              setShowPassword={setShowNewPassword}
              register={register}
              name="newPassword"
              error={errors.newPassword?.message}
            />
          </Box>
          <Box sx={{ marginY: 3 }}>
            <PasswordInput
              label="Confirm New Password"
              placeholder="Enter your new password again"
              showPassword={showConfirmNewPassword}
              setShowPassword={setShowConfirmNewPassword}
              register={register}
              name="confirmNewPassword"
              error={errors.confirmNewPassword?.message}
            />
          </Box>
          <SubmitButton
            disabled={
              !isErrorObjectEmpty(
                errors,
                newPassword,
                confirmNewPassword,
                currentPassword
              )
            }
            text="Change Password"
          />
        </form>
      </Paper>
    </Container>
  );
};

export default ChangePassword;
