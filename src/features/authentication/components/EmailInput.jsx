import { TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  textFieldFontSize: {
    fontSize: "1px",
    "& input": {
      fontSize: "1.1em",
      [theme.breakpoints.down("sm")]: {
        fontSize: "0.9em",
      },
    },
    "& input::placeholder": {
      fontSize: "1em",
      [theme.breakpoints.down("sm")]: {
        fontSize: "0.9em",
      },
    },
  },
}));

const EmailInput = ({ register, name, error }) => {
  const classes = useStyles();
  return (
    <TextField
      classes={{ root: classes.textFieldFontSize }}
      sx={{ width: "100%" }}
      label="Email"
      {...register(name)}
      error={!!error}
      helperText={error}
      placeholder="user@example.com"
    />
  );
};

export default EmailInput;
