import { Button } from "@mui/material";
import React from "react";

const SubmitButton = ({ disabled, text }) => {
  return (
    <Button
      type="submit"
      disabled={disabled}
      color="secondary"
      variant="contained"
      sx={{ width: "100%" }}
    >
      {text}
    </Button>
  );
};

export default SubmitButton;
