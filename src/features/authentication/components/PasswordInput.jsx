import { Visibility, VisibilityOff } from "@mui/icons-material";
import { IconButton, InputAdornment, TextField } from "@mui/material";
// import { makeStyles } from "@mui/styles";
import React from "react";

// const useStyles = makeStyles((theme) => ({
//   textFieldFontSize: {
//     fontSize: "1px",
//     "& input": {
//       fontSize: "1.1em",
//       [theme.breakpoints.down("sm")]: {
//         fontSize: "0.9em",
//       },
//     },
//     "& input::placeholder": {
//       fontSize: "1em",
//       [theme.breakpoints.down("sm")]: {
//         fontSize: "0.9em",
//       },
//     },
//   },
// }));

const PasswordInput = ({
  showPassword,
  setShowPassword,
  placeholder = "Enter your password",
  label = "Password",
  register,
  name,
  error,
}) => {
  return (
    <TextField
      type={showPassword ? "text" : "password"}
      variant="outlined"
      placeholder={placeholder}
      label={label}
      {...register(name)}
      error={!!error}
      helperText={error}
      fullWidth
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={() => setShowPassword(!showPassword)}
              onMouseDown={(e) => e.preventDefault()}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
};

export default PasswordInput;
